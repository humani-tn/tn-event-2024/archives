FROM node:lts-alpine3.16 AS build

WORKDIR /app

COPY app .
RUN npm install
RUN npm run build

FROM nginxinc/nginx-unprivileged 
COPY --from=build /app/build /usr/share/nginx/html
